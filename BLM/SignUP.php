<?php include('server.php') ?>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/SignUP.css">
    <title>Sign Up</title>
</head>
<body style="background: url(image/background.jpg);">
    <div class="container">
      <!-- action="/"---prevents direct summit -->
        <form id="form" class="form" action="SignUP.php" method="POST">
          <a href="index.php"><img src="image/back.png" alt=""></a>
          <?php include('error.php'); ?>
          <div class="form-content">
            <h2>Sign Up</h2>
              <div class="form-control">
                <input type = "text" id="username" name="username" placeholder = "User Name"
                class = "input-control" value="<?php echo $username; ?>"/>
                <small><!--error message--></small>
              </div>
             
              <div class="form-control">
                <input type="email" id="email" name="email" placeholder="Enter your email address"
                  class="input-control"  pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$" value="<?php echo $email; ?>"/>
                  <small><!--error message--></small>
              </div>
  
              <div class="form-control">
                <input type="password" id="password_1" name="password_1" placeholder="Enter your password" minlength="8"
                      class="input-control"/>
                <small><!--error message--></small>
              </div>
  
              <div class="form-control">
                <input type="password" id="password_2" name="password_2" placeholder="Confrim your Password"
                      class="input-control"/>
                    <small><!--error message--></small>
              </div>

              <div class="btnbox">
                <button type="submit" id="reg_user" name="reg_user">JOIN</button>
              </div> 
          </div>
        </form>
    </div>
    <!-- <script src="Ujss/SignUP.js"></script> -->
</body>
</html>
