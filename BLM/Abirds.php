<?php 
  session_start(); 

  if (!isset($_SESSION['email'])) {
  	$_SESSION['msg'] = "You must log in first";
  	header('location: index.php');
  }
  if (isset($_GET['logout'])) {
  	session_destroy();
  	unset($_SESSION['email']);
  	header("location: index.php");
  }
?>
<?php
  require_once 'server.php'; 
   
  // If file upload form is submitted 
  $status = $statusMsg = ''; 
  if(isset($_POST["submit1"])){ 
      $status = 'error'; 
      if(!empty($_FILES["image"]["name"])) { 
          // Get file info 
          $fileName = basename($_FILES["image"]["name"]); 
          $fileType = pathinfo($fileName, PATHINFO_EXTENSION); 
           
          // Allow certain file formats 
          $allowTypes = array('jpg','png','jpeg','gif'); 
          if(in_array($fileType, $allowTypes)){ 
              $image = $_FILES['image']['tmp_name']; 
              $imgContent = addslashes(file_get_contents($image)); 
           
              // Insert image content into database 
              $insert = $db->query("INSERT into images1 (image, created) VALUES ('$imgContent', NOW())");
               
              if($insert){ 
                  $status = 'success'; 
                  $statusMsg = "File uploaded successfully."; 
              }else{ 
                  $statusMsg = "File upload failed, please try again."; 
              }  
          }else{ 
              $statusMsg = 'Sorry, only JPG, JPEG, PNG, & GIF files are allowed to upload.'; 
          } 
      }else{ 
          $statusMsg = 'Please select an image file to upload.'; 
      } 
  } 
   
  // Display status message 
  echo $statusMsg; 
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="Acss/ADMINBIRDLM.css">
    <link rel="stylesheet" href="Acss/UHome.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css">

    <title>Bird life management</title>

</head>

<body>
    <header >
       <nav class="image1">
           <img class="image2" src="image/logo.png" alt="img" > 
           <img src="image/logo2.png" alt="img" style="width: 88%; height: 50%;">
        </nav>
        <nav class="navbar navbar-expand-lg navbar-light">
            <div class="container-fluid">
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                        <li class="nav-item">
                            <a class="nav-link active" aria-current="Page" href="AHome.php">Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" aria-current="Page" href="Abirds.php">Birds</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" aria-current="Page" href="Aaboutus.php">About US</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" aria-current="Page" href="status.php">Status</a>
                        </li>
                    </ul>
                </div>

                <div class="nav-right">
                    <div class="nav-user-icon" onclick="settingsMenuToogle()">
                        <img src="image/prolog.webp" alt="">
                    </div>
                </div>

                <div class="setting-menu">
                    <div class="settings-menu-innner">
                        <div class="user-profile">
                            <img src="image/prolog.webp" alt="">
                            <div>
                                <a href="Aeditprofile.php"><p> See your profile</p></a>
                            </div>
                        </div>
                        <hr>
                        <div class="user-profile">
                            <img src="image/star-fill.svg" alt="">
                            <div>
                                <a href="Arating.php"><p>Rating</p></a>
                            </div>
                        </div>
                        <hr>
                        <div class="user-profile">
                            <img src="image/door-open-fill.svg" alt="">
                            <div>
                                <a href="index.php"><p>log out</p></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </nav>
    </header>

    <div class="box">

        <div class="container2">
            <form class="search-bar" action="action_page.php">
                <input class="search-bar-1" type="text" placeholder="Search by birds, region" name="search">
                <button class="search-btn" type="submit" ><i class="fa fa-search" ></i></button>
            </form>
        </div>
        
        <div class="upload container-fluid">
            <div class="upload-container row">
                <div class="userP col-sm-3 p-3 ">
                    <img src="image/user.webp" class="user img-fluid" alt="">
                    <h4 class="name">ADMIN</h4>
                </div>
               
                <div class="upload-box col-sm-9 p-3 ">
                    <label for="exampleFormControlTextarea1" class="form-label">UPLOAD</label>
                    <br>
                    <div class="container-fluid">
                        <form method="POST" action="Abirds.php" enctype="multipart/form-data">
  	                            <input type="hidden" name="size" value="1000000">
  	                            <div>
  	                                <input type="file" name="image">
  	                            </div>
  	                            <div>
                                  <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" placeholder="Write something"></textarea>
  	                            </div>
  	                            <div>
  		                            <button type="submit" name="submit1" value="Upload">POST</button>
  	                            </div>
                            </form>

                    </div>
                  
                </div>
                
                
            </div>
        </div>
        <div class="grid-container container-fluid" >
            <div class="row">
                <div class="col">
                    <div class="birdimg">
                    <?php 
                    // Get image data from database 
                        $result = $db->query("SELECT image FROM images1 ORDER BY id DESC"); 
                    ?>

                    <?php if($result->num_rows > 0){ ?> 
                    <?php }else{ ?> 
                        <p class="status error">Image(s) not found...</p> 
                    <?php } ?>
                    <?php while($row = $result->fetch_assoc()){ ?> 
                        <img src="data:image/jpg;charset=utf8;base64,<?php echo base64_encode($row['image']); ?>" 
                        style="width: 200px; height: 200px"/> 
                        <hr>
                    <?php } ?> 
                    </div>
                    <div class="birdname">
                        <h4 class="name">Bar-winged Wren Babblers</h4>
                    </div>
                </div>
        
               
                <div class="col">
                    <div class="birdimg">
                        <a href="Abird.php"><img src="BIRD/bird1.jpg" class="img-fluid" alt=""></a>
                    </div>
                    <div class="birdname">
                        <h4 class="name">Gould's shortwing</h4>
                     </div>
                </div>
                
                <div class="col">
                    <div class="birdimg">
                        <a href="Abird4.html"><img src="BIRD/bird4.jpg" class="img-fluid" alt=""></a>
                    </div>
                    <div class="birdname">
                        <h4 class="name">Yellow-rumped Honeyguide</h4>
                     </div>
                    
                </div>
                <div class="col">
                    <div class="birdimg">
                        <a href="Abird5.html"><img src="BIRD/bird5.jpg" class="img-fluid" alt=""></a>
                    </div>
                    <div class="birdname">
                        <h4 class="name">Black-headed Shrike Babbler</h4>
                     </div>
                </div>

            </div>

            <div class="row">
                <div class="col">
                    <div class="birdimg">
                        <a href="Abird6.html"><img src="BIRD/bird6.jpg" class="img-fluid" alt=""></a>
                    </div>
                    <div class="birdname">
                        <h4 class="name">Shrike Babbler</h4>
                     </div>
                </div>

                <div class="col">
                    <div class="birdimg">
                        <a href="Abird7.html"><img src="BIRD/bird7.jpg" class="img-fluid" alt=""></a>
                    </div>
                    <div class="birdname">
                        <h4 class="name">Ward's Trogon</h4>
                     </div>
                </div>

                <div class="col">
                    <div class="birdimg">
                        <a href="Abird8.html"><img src="BIRD/bird8.jpg" class="img-fluid" alt=""></a>
                    </div>
                    <div class="birdname">
                        <h4 class="name">Grey-winged Blackbird</h4>
                     </div>
                </div>

                <div class="col">
                    <div class="birdimg">
                        <a href="Abird9.html"><img src="BIRD/bird9.jpg" class="img-fluid" alt=""></a>
                    </div>
                    <div class="birdname">
                        <h4 class="name">Scarlet Minivet</h4>
                     </div>
                </div>
                
                <div class="col">
                    <div class="birdimg">
                        <a href="Abird10.html"><img src="BIRD/bird10.jpg" class="img-fluid" alt=""></a>
                    </div>
                    <div class="birdname">
                        <h4 class="name"> Nuthatch</h4>
                     </div>
                </div>

            </div>

            <div class="row">
                <div class="col">
                    <div class="birdimg">
                        <a href="Abird11.html"><img src="BIRD/bird11.jpg" class="img-fluid" alt=""></a>
                    </div>
                    <div class="birdname">
                        <h4 class="name">Black-throated Prinia</h4>
                     </div>
                </div>

            </div>
        </div> 

    </div>

  

    <footer class="footer">
        <div class="row">
          <div class="col-sm-4">
            <h5>Location</h5>
            Mongar: BHUTAN
            <br>Yongkola Bird life Management
          </div>
          <div class="col-sm-4">
            <h5>Follow Us</h5>
            <div class="social-links">
              <a href="#"><i class="fab fa-facebook-f px-1"></i></a>
              <a href="#"><i class="fab fa-twitter px-1"></i></a>
              <a href="#"><i class="fab fa-instagram px-1"></i></a>
              <a href="#"><i class="fab fa-tiktok px-1"></i></a>
            </div>
          </div>
          <div class="col-sm-4">
            <h5>Contact Us</h5>
            <address>
              <a href="">Yongkolabirdlife.com</a><br>
              <hr>
              <a href="tel:17777777">17777777</a>
            </address>
          </div>
        </div>
      </footer>
    
<script src="Ajss/UHome.js"></script>

</body>
</html>