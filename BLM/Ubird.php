<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="Ucss/UHome.css">
    <link rel="stylesheet" href="Ucss/UBLM.css">
    <link rel="stylesheet" href="Ucss/UABLM.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css">

    <title>Bird life management</title>

</head>

<body>
    <header >
       <nav class="image1">
           <img class="image2" src="image/logo.png" alt="img" > 
           <img src="image/logo2.png" alt="img" style="width: 88%; height: 50%;">
        </nav>
        <nav class="navbar navbar-expand-lg navbar-light">
            <div class="container-fluid">
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                        <li class="nav-item">
                            <a class="nav-link active" aria-current="Page" href="Home.php">Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" aria-current="Page" href="Ubird1.php">Birds</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" aria-current="Page" href="Ua.php">About US</a>
                        </li>
                    </ul>
                </div>

                <div class="nav-right">
                    <div class="nav-user-icon" onclick="settingsMenuToogle()">
                        <img src="image/prolog.webp" alt="">
                    </div>
                </div>

                <div class="setting-menu">
                    <div class="settings-menu-innner">
                        <div class="user-profile">
                            <img src="image/prolog.webp" alt="">
                            <div>
                                <a href="Ueditprofile.php"><p> See your profile</p></a>
                            </div>
                        </div>
                        <hr>
                        <div class="user-profile">
                            <img src="image/star-fill.svg" alt="">
                            <div>
                                <a href="Urating.php"><p>Rating</p></a>
                            </div>
                        </div>
                        <hr>
                        <hr>
                        <div class="user-profile">
                            <a href="index.php"><img src="image/door-open-fill.svg" alt=""></a>
                            <div>
                                <a href="index.php"><p>log out</p></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </nav>
    </header>

    

    <div class="box">
        <div class="info-box">
            <div class="container2">
                <form class="search-bar" action="action_page.php">
                    <input class="search-bar-1" type="text" placeholder="Search by birds, region" name="search">
                    <button class="search-btn" type="submit" ><i class="fa fa-search" ></i></button>
                </form>
            </div>
            <br>
            
            <div class="details container-fluid">
                 <a href="Ubird1.php" class="previous round">
                    <button class="back-button" type="submit" >&#8249;</button>
                </a>
                <div class="dbird1">
                    <div class="dbirdimg">
                        <a ><img src="BIRD/bird1.jpg" class="img-fluid" alt=""  ; >
                        </a>
                    </div>
                    <div class="dbirdname">
                        
                        <h4 class="name">Cuculus canorus</h4>
                    </div>
                </div>
                <br>
                <div class="birdinfo">
                    <p>
                        The common cuckoo (Cuculus canorus) is a member of the cuckoo order of birds, Cuculiformes, which includes the roadrunners, the anis and the coucals.
                        The longest recorded lifespan of a common cuckoo in the United Kingdom is 6 years, 11 months and 2 days.
                    </p>
                    <p>
                        The common cuckoo is 32–34 centimetres (13–13 in) long from bill to tail (with a tail of 13–15 centimetres (5.1–5.9 in) and a wingspan of 55–60 centimetres (22–24 in).[4] The legs are short. It has a greyish, slender body and long tail, similar to a sparrowhawk in flight, where the wingbeats are regular. During the breeding season, common cuckoos often settle on an open perch with drooped wings and raised tail. There is a rufous colour morph, which occurs occasionally in adult females but more often in juveniles.
                    </p>
                </div>
            </div>
        </div>
    </div>
    <footer class="footer">
        <div class="row">
          <div class="col-sm-4">
            <h5>Location</h5>
            Mongar: BHUTAN
            <br>Yongkola Bird life Management
          </div>
          <div class="col-sm-4">
            <h5>Follow Us</h5>
            <div class="social-links">
              <a href="#"><i class="fab fa-facebook-f px-1"></i></a>
              <a href="#"><i class="fab fa-twitter px-1"></i></a>
              <a href="#"><i class="fab fa-instagram px-1"></i></a>
              <a href="#"><i class="fab fa-tiktok px-1"></i></a>
            </div>
          </div>
          <div class="col-sm-4">
            <h5>Contact Us</h5>
            <address>
              <a href="">Yongkolabirdlife.com</a><br>
              <hr>
              <a href="tel:17777777">17777777</a>
            </address>
          </div>
        </div>
      </footer>

      <script src="Ujss/UHome.js"></script>
</body>
</html>


