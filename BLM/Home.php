<?php 
  session_start(); 

  if (!isset($_SESSION['email'])) {
  	$_SESSION['msg'] = "You must log in first";
  	header('location: index.php');
  }
  if (isset($_GET['logout'])) {
  	session_destroy();
  	unset($_SESSION['email']);
  	header("location: index.php");
  }
?>
<?php
  require_once 'server.php';
  // If file upload form is submitted 
  $status = $statusMsg = ''; 
  if(isset($_POST["submit"])){ 
      $status = 'error'; 
      if(!empty($_FILES["image"]["name"])) { 
          // Get file info 
          $fileName = basename($_FILES["image"]["name"]); 
          $fileType = pathinfo($fileName, PATHINFO_EXTENSION); 
           
          // Allow certain file formats 
          $allowTypes = array('jpg','png','jpeg','gif'); 
          if(in_array($fileType, $allowTypes)){ 
              $image = $_FILES['image']['tmp_name']; 
              $imgContent = addslashes(file_get_contents($image)); 
           
              // Insert image content into database 
              $insert = $db->query("INSERT into images (image, created) VALUES ('$imgContent', NOW())");
               
              if($insert){ 
                  $status = 'success'; 
                  $statusMsg = "File uploaded successfully."; 
              }else{ 
                  $statusMsg = "File upload failed, please try again."; 
              }  
          }else{ 
              $statusMsg = 'Sorry, only JPG, JPEG, PNG, & GIF files are allowed to upload.'; 
          } 
      }else{ 
          $statusMsg = 'Please select an image file to upload.'; 
      } 
  } 
   
  // Display status message 
  echo $statusMsg; 
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="Ucss/UHome.css">
    <link rel="stylesheet" href="Ucss/https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css">
    <title>Bird life management</title>

</head>

<body>
    <header >
       <nav class="image1">
           <img class="image2" src="image/logo.png" alt="img" > 
           <img src="image/logo2.png" alt="img" style="width: 88%; height: 50%;">
        </nav>
        <nav class="navbar navbar-expand-lg navbar-light">
            <div class="container-fluid">
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                        <li class="nav-item">
                            <a class="nav-link active" aria-current="Page" href="Home.php">Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" aria-current="Page" href="Ubird1.php">Birds</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" aria-current="Page" href="Ua.php">About US</a>
                        </li>
                    </ul>
                </div>

                <div class="nav-right">
                    <div class="nav-user-icon"  onclick="settingsMenuToogle()">
                        <img src="image/prolog.webp" alt="">
                    </div>
                </div>

                <div class="setting-menu">
                    <div class="settings-menu-innner">
                        <div class="user-profile">
                            <img src="image/prolog.webp" alt="">
                            <div>
                                <a href="Ueditprofile.php"><p> See your profile</p></a>
                            </div>
                        </div>
                        <hr>
                        <div class="user-profile">
                            <img src="image/star-fill.svg" alt="">
                            <div>
                                <a href="Urating.php"><p>Rating</p></a>
                            </div>
                        </div>
                        <hr>
                        <div class="user-profile">
                            <a href="index.php"><img src="image/door-open-fill.svg" alt=""></a>
                            <div>
                                <a href="index.php"><p>log out</p></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </nav>
    </header>

    <!-- Sections Starts -->
    <section class="nav-body">
        <div class="container">
            <div class="left-sidebar">
                <div>
                    <img src="image/hunt.jpg" alt="">
                    <p>“If you listen to birds, every day will have a song in it.” <br> —Kyo Maclear</p>
                </div>
            </div>
            <div class="main-sidebar">
                <div class="post-in-put-container">  
                    <div class="user-profile">
                        <img src="image/prolog.webp" alt="">
                        <div>
                            <p>John Nichole</p>
                        </div>
                    </div>
                    <div class="nav-container" id="toggleBtn1">
                        <p>Say something John!</p>
                    </div>
                </div>
                <div class="setting-post">
                    <div class="post-menu-inner">
                        <div class="create-post">
                            <h1>Create Post</h1>
                            <hr>
                            <form method="POST" action="Home.php" enctype="multipart/form-data">
  	                            <input type="hidden" name="size" value="1000000">
  	                            <div>
  	                                <input type="file" name="image">
  	                            </div>
  	                            <div>
                                  <textarea id="text" cols="40" rows="4" name="name" placeholder="Say something about this image..."></textarea>
  	                            </div>
  	                            <div>
  		                            <button type="submit" name="submit" value="Upload">POST</button>
  	                            </div>
                            </form>
                          </div>
                    </div>
                </div>
                <div class="post-container">
                    <div class="user-profile">
                        <img src="image/prolog.webp" alt="">
                        <div>
                            <p>Alex</p>
                            <span>May 2 2022, 7:40am</span>
                        </div>
                    </div>
                    <?php 
                        // Get image data from database 
                        $result = $db->query("SELECT image FROM images ORDER BY id DESC"); 
                    ?>

                    <?php if($result->num_rows > 0){ ?> 
                    <?php }else{ ?> 
                        <p class="status error">Image(s) not found...</p> 
                    <?php } ?>
                    <p>Birds are vertebrate animals adapted for flight. Many can also run, jump, swim, and dive. Some, like penguins, have lost the ability to fly but retained their wings.</p>
                    <?php while($row = $result->fetch_assoc()){ ?> 
                            <img src="data:image/jpg;charset=utf8;base64,<?php echo base64_encode($row['image']); ?>" 
                            style="width: 100%; height: auto;"/> 
                            <hr>
                        <?php } ?> 

                    <div class="post-row">
                        <div class="like-icons">
                            <div><img src="image/heart-fill.svg" >4K</div>
                        </div>
                    </div>
                </div>
                <div class="post-container">
                    <div class="user-profile">
                        <img src="image/prolog.webp" alt="">
                        <div>
                            <p>John Nichole</p>
                            <span>May 2 2022, 11:40pm</span>
                        </div>
                    </div>
                    <p>I am very happy to see this bird. Its on of the rare bird in Bhutan.</p>
                    <img src="image/bird.jpg" class="post-img">
                    <div class="post-row">
                        <div class="like-icons">
                            <div><img src="image/heart-fill.svg" >4K</div>
                        </div>
                    </div>
                </div>
                <div class="post-container">
                    <div class="user-profile">
                        <img src="image/prolog.webp" alt="">
                        <div>
                            <p>Alex</p>
                            <span>May 2 2022, 7:40am</span>
                        </div>
                    </div>
                    <p>Birds are vertebrate animals adapted for flight. Many can also run, jump, swim, and dive. Some, like penguins, have lost the ability to fly but retained their wings.</p>
                    <img src="image/bird1.jpg" class="post-img">
                    <div class="post-row">
                        <div class="like-icons">
                            <div><img src="image/heart-fill.svg" >2K</div>
                        </div>
                    </div>
                </div>
                <button type="button" class="Load-more-btn">Load More</button>
            </div>

            <div class="right-sidebar ">
                <h1>Related: </h1>
                <hr>
                <h2><img src="image/dot.svg" alt=""><a href="http://www.rspn.org/">Royal Society For Protection Of Nature</a></h2>
                <h3><img src="image/dot.svg" alt=""><a href="http://birdsinternational.net">Bird International</a></h3>
            </div>
        </div>
    </section>
    <!-- Sections ends -->
    <footer class="footer">
        <div class="row">
          <div class="col-sm-4">
            <h5>Location</h5>
            Mongar: BHUTAN
            <br>Yongkola Bird life Management
          </div>
          <div class="col-sm-4">
            <h5>Follow Us</h5>
            <div class="social-links">
              <a href="#"><i class="fab fa-facebook-f px-1"></i></a>
              <a href="#"><i class="fab fa-twitter px-1"></i></a>
              <a href="#"><i class="fab fa-instagram px-1"></i></a>
              <a href="#"><i class="fab fa-tiktok px-1"></i></a>
            </div>
          </div>
          <div class="col-sm-4">
            <h5>Contact Us</h5>
            <address>
              <a href="">Yongkolabirdlife.com</a><br>
              <hr>
              <a href="tel:17777777">17777777</a>
            </address>
          </div>
        </div>
    </footer>

  
    <script src="Ujss/UHome.js"></script>
</body>
</html>