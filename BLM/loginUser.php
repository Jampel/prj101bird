<?php include('server.php') ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Bird Life</title>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="css/login.css">
    <script defer src="jss/form.js"></script>
</head>
<body style="background: url(image/background.jpg)">
    <div class="container" >

        <form id="form" action="loginUser.php" method="POST">
        <?php include('error.php'); ?>
         
         <a href="index.php"><img src="image/back.png" alt=""></a>
              
        
            
            <h1>Login</h1>
            <div class="input-control">
                <label for="email">Email</label>
                <input id="email" name="email" type="text">
                <div class="error"></div>
            </div>
            <div class="input-control">
                <label for="password">Password</label>
                <input id="password" name="password" type="password">
                <div class="error"></div>
            </div>
        
            <button type="submit" name="login_user">Log In</button><hr>
           
            
        </form>
    </div>
</body>
</html>
