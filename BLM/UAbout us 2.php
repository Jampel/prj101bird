<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css">    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="Ucss/UHome.css">
    <link rel="stylesheet" href="Ucss/UAbout.css">
    <title>Bird life management</title>
</head>
<body>
    <header >
        <nav class="image1">
            <img class="image2" src="image/logo.png" alt="img" > 
            <img src="image/logo2.png" alt="img" style="width: 88%; height: 50%;">
         </nav>
         <nav class="navbar navbar-expand-lg navbar-light">
             <div class="container-fluid">
                 <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                     <span class="navbar-toggler-icon"></span>
                 </button>
                 <div class="collapse navbar-collapse" id="navbarSupportedContent">
                     <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                         <li class="nav-item">
                             <a class="nav-link active" aria-current="Page" href="Home.php">Home</a>
                         </li>
                         <li class="nav-item">
                             <a class="nav-link active" aria-current="Page" href="Ubird1.php">Birds</a>
                         </li>
                         <li class="nav-item">
                             <a class="nav-link active" aria-current="Page" href="Ua.php">About US</a>
                         </li>
                     </ul>
                 </div>
 
                 <div class="nav-right">
                     <div class="nav-user-icon" onclick="settingsMenuToogle()">
                         <img src="image/prolog.webp" alt="">
                     </div>
                 </div>
 
                 <div class="setting-menu">
 
                     <div id="dark-btn">
                         <span></span>
                     </div>
                     <div class="settings-menu-innner">
                         <div class="user-profile">
                             <img src="image/prolog.webp" alt="">
                             <div>
                                 <a href="Ueditprofile.php"><p> See your profile</p></a>
                             </div>
                         </div>
                         <hr>
                         <div class="user-profile">
                             <img src="image/star-fill.svg" alt="">
                             <div>
                                 <a href="Urating.php"><p>Rating</p></a>
                             </div>
                         </div>
                         <hr>
                         <div class="user-profile">
                            <img src="image/door-open-fill.svg" alt="">
                            <div>
                                <a href="index.php"><p>log out</p></a>
                            </div>
                        </div>
                         <hr>
                         <div class="user-profile">
                             <a href="index.php"><img src="image/door-open-fill.svg" alt=""></a>
                             <div>
                                 <a href="index.php"><p>log out</p></a>
                             </div>
                         </div>
                     </div>
                 </div>
             </div>
         </nav>
     </header>
    <!-- About us sections  -->
    <div class="nav-body" style="background-image:url(image/abt.jpg) ;">
        <div class="container">
           <div class="text">
               
               <h2>Background Information</h2>
               <p>Bhutan is home to different bird species which also makes it up for her to be recognized as one 
                of the global hotspots. To be very specific Yonkola, under Mongar dzongkhag serves as a 
                habitat to many bird species in Bhutan due to its moderate climate and other favorable factors 
                in bringing the attention of educational tourists and some of that eco-tourist who are into 
                nature. However, it is pointed out that there could be the possibility of them either migrating 
                from this region to other or getting extinct because of very poor management and advancement 
                level is far behind to be supporting the lives of birds in terms of technology. For instance, the 
                very Ivory-billed woodpecker also to be known as “Lord God Bird” is now no longer in 
                existence since 2021 due to a change in its external factor which made it difficult for them to 
                be able to adjust and adapt. </p>
                   <a href="Ua.php" class="btn btn-primary">Go Back</a>
           </div> 
        </div>
    </div>
    <footer class="footer">
        <div class="row">
          <div class="col-sm-4">
            <h5>Location</h5>
            Mongar: BHUTAN
            <br>Yongkola Bird life Management
          </div>
          <div class="col-sm-4">
            <h5>Follow Us</h5>
            <div class="social-links">
              <a href="#"><i class="fab fa-facebook-f px-1"></i></a>
              <a href="#"><i class="fab fa-twitter px-1"></i></a>
              <a href="#"><i class="fab fa-instagram px-1"></i></a>
              <a href="#"><i class="fab fa-tiktok px-1"></i></a>
            </div><br>
            
          </div>
          <div class="col-sm-4">
            <h5>Contact Us</h5>
            <address>
              <a href="">Yongkolabirdlife.com</a><br>
              <hr>
              <a href="tel:17777777">17777777</a>
            </address>
          </div>
        </div>
      </footer>
<!-- About Us ends -->
    <script src="Ujss/UHome.js"></script>
</body>
</html>