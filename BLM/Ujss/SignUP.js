const form = document.getElementById("form");
const firstname = document.getElementById("Fname");
const lastname = document.getElementById("Lname");
const email = document.getElementById("email");
const password = document.getElementById("password");
const password2 = document.getElementById("password2");

    function showError(input, message) {
        const formControl = input.parentElement;
        formControl.className = "form-control error";
        const small = formControl.querySelector("small");
        small.innerText = message;
    }
    function showSuccess(input) {
        const formControl = input.parentElement;
        formControl.className = "form-control success";
        const small = formControl.querySelector("small");
        small.innerText = "";
    }
    function emailValidation(email) {
        const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }
    function checkPasswordsMatch(input1, input2) {
        if (input1.length < 8){
			showError(input1, "Passwords should have minimum of 8 character!");
        }else if (input1.value !== input2.value) {
        	showError(input2, "Passwords do not match!");
		}

        if (input1.value === input2.value) {
            const formControl = input2.parentElement;
        formControl.className = "form-control success";
            const small = formControl.querySelector("small");
            small.innerText = "Password match";
        }
    }
        
	
form.addEventListener("submit", function(e) {
    e.preventDefault(); //when we click on submit, it just submits, but we don't want that, we want to hold for a while
    if (firstname.value === "") {
    	showError(firstname, "*Firstname is required");
    } 
    else {
        showSuccess(firstname);
    }
	if (lastname.value === "") {
    	showError(lastname, "*Lastname is required");
    } else {
        showSuccess(lastname);
    }
                                      
    if (email.value === "") {
        showError(email, "*Email is required");
    } else if (!emailValidation(email.value)) {
        showError(email, "*Email is not valid");
    } else {
        showSuccess(email);
    }
                                      
    if (password.value === "") {
        showError(password, "*Password is required");
    } else {
        showSuccess(password);
    }
                                     
    if (password2.value === "") {
        showError(password2, "*Password confrimation is required");
    } else {
        showSuccess(password2);
    }
     checkPasswordsMatch(password, password2);
});