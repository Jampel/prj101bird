<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="Acss/ADMINBIRDLM.css">
    <link rel="stylesheet" href="Acss/UHome.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css">

    <title>Bird life management</title>

</head>

<body>
    <header >
       <nav class="image1">
           <img class="image2" src="image/logo.png" alt="img" > 
           <img src="image/logo2.png" alt="img" style="width: 88%; height: 50%;">
        </nav>
        <nav class="navbar navbar-expand-lg navbar-light">
            <div class="container-fluid">
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                        <li class="nav-item">
                            <a class="nav-link active" aria-current="Page" href="AHome.php">Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" aria-current="Page" href="Abirds.php">Birds</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" aria-current="Page" href="Aaboutus.php">About US</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" aria-current="Page" href="status.php">Status</a>
                        </li>
                    </ul>
                </div>

                <div class="nav-right">
                    <div class="nav-user-icon" onclick="settingsMenuToogle()">
                        <img src="image/prolog.webp" alt="">
                    </div>
                </div>

               
                <div class="setting-menu">
                    <div class="settings-menu-innner">
                        <div class="user-profile">
                            <img src="image/prolog.webp" alt="">
                            <div>
                                <a href="Aeditprofile.php"><p> See your profile</p></a>
                            </div>
                        </div>
                        <hr>
                        <div class="user-profile">
                            <img src="image/star-fill.svg" alt="">
                            <div>
                                <a href="Arating.php"><p>Rating</p></a>
                            </div>
                        </div>
                        <hr>
                        <div class="user-profile">
                            <img src="image/door-open-fill.svg" alt="">
                            <div>
                                <a href="index.php"><p>log out</p></a>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
        </nav>
    </header>

    
    <div class="Scontainer container-fluid" >
        <div class="SBox">
            <div class="Status-box">
                <div class="SB row">
                    <div class="boxes col-sm-4">
                        <a href="user.php">
                            <button type="button" class="btn btn-outline-success">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-emoji-smile-fill" viewBox="0 0 16 16">
                                <path d="M8 16A8 8 0 1 0 8 0a8 8 0 0 0 0 16zM7 6.5C7 7.328 6.552 8 6 8s-1-.672-1-1.5S5.448 5 6 5s1 .672 1 1.5zM4.285 9.567a.5.5 0 0 1 .683.183A3.498 3.498 0 0 0 8 11.5a3.498 3.498 0 0 0 3.032-1.75.5.5 0 1 1 .866.5A4.498 4.498 0 0 1 8 12.5a4.498 4.498 0 0 1-3.898-2.25.5.5 0 0 1 .183-.683zM10 8c-.552 0-1-.672-1-1.5S9.448 5 10 5s1 .672 1 1.5S10.552 8 10 8z"></path>
                                </svg>
                                USER
                            </button>

                        </a>
                       
                    </div>
                    <div class="boxes col-sm-4">
                        <a href="Abirds.php">
                            <button type="button" class="btn btn-outline-secondary">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-twitter" viewBox="0 0 16 16">
                                <path d="M5.026 15c6.038 0 9.341-5.003 9.341-9.334 0-.14 0-.282-.006-.422A6.685 6.685 0 0 0 16 3.542a6.658 6.658 0 0 1-1.889.518 3.301 3.301 0 0 0 1.447-1.817 6.533 6.533 0 0 1-2.087.793A3.286 3.286 0 0 0 7.875 6.03a9.325 9.325 0 0 1-6.767-3.429 3.289 3.289 0 0 0 1.018 4.382A3.323 3.323 0 0 1 .64 6.575v.045a3.288 3.288 0 0 0 2.632 3.218 3.203 3.203 0 0 1-.865.115 3.23 3.23 0 0 1-.614-.057 3.283 3.283 0 0 0 3.067 2.277A6.588 6.588 0 0 1 .78 13.58a6.32 6.32 0 0 1-.78-.045A9.344 9.344 0 0 0 5.026 15z"></path>
                                </svg>
                               NO. of BIRDS
                              </button>
                            

                        </a>
                    </div>
                    
                </div>
                <br>
                <div class="SB row">
                    
                    <div class="boxes col-sm-4">
                        <a href="Arating.php">
                            <button type="button" class="btn btn-outline-dark" >
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-star-half" viewBox="0 0 16 16">
                                <path d="M5.354 5.119 7.538.792A.516.516 0 0 1 8 .5c.183 0 .366.097.465.292l2.184 4.327 4.898.696A.537.537 0 0 1 16 6.32a.548.548 0 0 1-.17.445l-3.523 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256a.52.52 0 0 1-.146.05c-.342.06-.668-.254-.6-.642l.83-4.73L.173 6.765a.55.55 0 0 1-.172-.403.58.58 0 0 1 .085-.302.513.513 0 0 1 .37-.245l4.898-.696zM8 12.027a.5.5 0 0 1 .232.056l3.686 1.894-.694-3.957a.565.565 0 0 1 .162-.505l2.907-2.77-4.052-.576a.525.525 0 0 1-.393-.288L8.001 2.223 8 2.226v9.8z"></path>
                                </svg>
                                RATING
                            </button>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

  

    <footer class="footer">
        <div class="row">
          <div class="col-sm-4">
            <h5>Location</h5>
            Mongar: BHUTAN
            <br>Yongkola Bird life Management
          </div>
          <div class="col-sm-4">
            <h5>Follow Us</h5>
            <div class="social-links">
              <a href="#"><i class="fab fa-facebook-f px-1"></i></a>
              <a href="#"><i class="fab fa-twitter px-1"></i></a>
              <a href="#"><i class="fab fa-instagram px-1"></i></a>
              <a href="#"><i class="fab fa-tiktok px-1"></i></a>
            </div>
          </div>
          <div class="col-sm-4">
            <h5>Contact Us</h5>
            <address>
              <a href="">Yongkolabirdlife.com</a><br>
              <hr>
              <a href="tel:17777777">17777777</a>
            </address>
          </div>
        </div>
      </footer>
    
    <script src="Ajss/UHome.js"></script>

</body>
</html>