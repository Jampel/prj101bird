<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="Acss/ADMINBIRDLM.css">
    <link rel="stylesheet" href="Acss/UHome.css">
    <link rel="stylesheet" href="Acss/Arating.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css">

    <title>Bird life management</title>

</head>

<body>
    <header >
       <nav class="image1">
           <img class="image2" src="image/logo.png" alt="img" > 
           <img src="image/logo2.png" alt="img" style="width: 88%; height: 50%;">
        </nav>
        <nav class="navbar navbar-expand-lg navbar-light">
            <div class="container-fluid">
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                        <li class="nav-item">
                            <a class="nav-link active" aria-current="Page" href="AHome.php">Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" aria-current="Page" href="Abirds.php">Birds</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" aria-current="Page" href="Aaboutus.php">About US</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" aria-current="Page" href="status.php">Status</a>
                        </li>
                    </ul>
                </div>

                <div class="nav-right">
                    <div class="nav-user-icon" onclick="settingsMenuToogle()">
                        <img src="image/prolog.webp" alt="">
                    </div>
                </div>

               
                <div class="setting-menu">
                    <div class="settings-menu-innner">
                        <div class="user-profile">
                            <img src="image/prolog.webp" alt="">
                            <div>
                                <a href="Aeditprofile.php"><p> See your profile</p></a>
                            </div>
                        </div>
                        <hr>
                        <div class="user-profile">
                            <img src="image/star-fill.svg" alt="">
                            <div>
                                <a href="Arating.php"><p>Rating</p></a>
                            </div>
                        </div>
                        <hr>
                        <div class="user-profile">
                            <img src="image/door-open-fill.svg" alt="">
                            <div>
                                <a href="index.php"><p>log out</p></a>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
        </nav>
    </header>

    
    <div class="Rcontainer container-fluid " style="background-color:rgb(255, 255, 255) ;">
        <div class="rbox ">
            <div class="row">
                <div class="t1 col-md-1" style="align-items: center; justify-content: center; display: flex;">
                    <button class="back-button" type="submit" ><a href="status.php" class="previous round">&#8249;</a></button>
                </div>
                <div class="t2 col">
                    
                    <h1 style="text-align: center;">Ratings</h1>
                </div>
            </div>
            
            <div class="rbox1 col">
                <hr>
                <div class="row">
                    <img src="image/r1.png" alt="">

                    <div class="col">
                       <div class="col-md-2"> 
                        <img src="image/s1.png" alt="">
                        <img src="image/s1.png" alt="">
                        <img src="image/s1.png" alt="">
                        <img src="image/s2.png" alt="">
                        <img src="image/s3.jpg" alt="">
                       </div> 
                        <p>information about birds are really easy to access now!</p>
                        <p>love the website</p>

                    </div>

                </div>
                <hr>
                <div class="row">
                    <img src="image/r3.png" alt="">

                    <div class="col">
                        <div class="col-md-2"> 
                            <img src="image/s1.png" alt="">
                            <img src="image/s1.png" alt="">
                            <img src="image/s1.png" alt="">
                            <img src="image/s3.jpg" alt="">
                            <img src="image/s3.jpg" alt="">
                        </div> 
                        <p>love the website but the design could have been better</p>
                        <p></p>
                        
                    </div>

                </div>
                <hr>
                <div class="row">
                    <img src="image/r4.png" alt="">

                    <div class="col">
                        <div class="col-md-2"> 
                            <img src="image/s1.png" alt="">
                            <img src="image/s1.png" alt="">
                            <img src="image/s1.png" alt="">
                            <img src="image/s1.png" alt="">
                            <img src="image/s3.jpg" alt="">
                        </div> 
                        <p>loved the fact that now information about birds are easy to get </p>
                        <p>#lovethewebsite </p>
                        
                    </div>

                </div>
                <hr>
                <div class="row">
                    <img src="image/r1.png" alt="">

                    <div class="col">
                        <div class="col-md-2"> 
                            <img src="image/s1.png" alt="">
                            <img src="image/s1.png" alt="">
                            <img src="image/s2.png" alt="">
                            <img src="image/s3.jpg" alt="">
                            <img src="image/s3.jpg" alt="">
                        </div> 
                        <p>informations are missing </p>
                        <p>need more info, other then that loved the website</p>
                        
                    </div>

                </div>
                <hr>
                <div class="row">
                    <img src="image/r3.png" alt="">
                    
                    <div class="col">
                        <div class="col-md-2"> 
                            <img src="image/s1.png" alt="">
                            <img src="image/s1.png" alt="">
                            <img src="image/s1.png" alt="">
                            <img src="image/s1.png" alt="">
                            <img src="image/s1.png" alt="">
                        </div> 
                        <p>wow!</p>
                        <p>i really loved the website</p>
                        
                    </div>
                    <hr>

                </div>
                
            </div>

        </div>
        
        
    </div>

  

    <footer class="footer">
        <div class="row">
          <div class="col-sm-4">
            <h5>Location</h5>
            Mongar: BHUTAN
            <br>Yongkola Bird life Management
          </div>
          <div class="col-sm-4">
            <h5>Follow Us</h5>
            <div class="social-links">
              <a href="#"><i class="fab fa-facebook-f px-1"></i></a>
              <a href="#"><i class="fab fa-twitter px-1"></i></a>
              <a href="#"><i class="fab fa-instagram px-1"></i></a>
              <a href="#"><i class="fab fa-tiktok px-1"></i></a>
            </div>
          </div>
          <div class="col-sm-4">
            <h5>Contact Us</h5>
            <address>
              <a href="">Yongkolabirdlife.com</a><br>
              <hr>
              <a href="tel:17777777">17777777</a>
            </address>
          </div>
        </div>
      </footer>
    
    <script src="Ajss/UHome.js"></script>

</body>
</html>