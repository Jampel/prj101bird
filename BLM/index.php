<?php include('server.php') ?>
<!doctype html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
    integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
    integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM"
    crossorigin="anonymous"></script>


  <link rel="stylesheet" href="css/LP.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css"
    integrity="sha512-+4zc9k=qNFUR5X+cKL9EIR=ZOhtIloNl9GIKS57VlMyNsYpYcUrUeQc9vNfzsWfV28IaLL3i96PsdNyeRssa==)"
    copyright="anonymous">
  <title>BirdLife Management</title>
  <nav class="navbar navbar-expand-lg navbar-dark  ">
    <div class="container-fluid">
      <a class="blogo navbar-brand fw-bold" href="#">
          <img src="image/lopgo 19.png" alt="">
      </a>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav"
      aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
     
      <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav ms-auto">
          <li class="nav-item">
            <a class="nav-link active" aria-current="page" href="SignUP.php">Sign up</a>
          </li>
          <li class="nav-item">
            <a class="nav-link active" href="loginUser.php">Login</a>
          </li>


        </ul>
      </div>
    </div>
  </nav>
</head>

<body>
  <div id="home" style="
  background: url(image/display.gif) no-repeat center center fixed;
    display: table;
    position: relative;
    background-size: cover;
    width: 100%;
    height: 50%;
    color: aliceblue;
  ">
    <div class="landing-text vh-100 text-center">
      <i class="fas fa-quote-left my-4"></i>
      <strong>
        <p class="test1">Birds teach a great life lesson. All you have to do is listen to their song
        </p>
      </strong>
      <p class="author text-white">-Unknown</p>
      
    </div>
  </div>

  <div class="padding">
    <div class="container">
      <div class="row">
        <div class="col-sm-6">
          <img src="image/yongkola.png">
        </div>
        <div class="col-sm-6 text-center">
          <h1 class="heading">Yongkola Birdlife Management</h1>
          <div class="article1">
            <p>Bhutan is home to different bird species which also makes it up for her to be recognized as one of global hotspot. 
                To be very specific Yongkola, under Mongar dzongkhag serves as habitat to many bird species in Bhutan due to its moderate climate and other favorable factors in bringing the attention of educational tourist and some of those eco tourist who are into nature.
            </p>
           
          </div>
          <div class="articlie2">
            <p>
                According to the Royal Society for the Protection of Nature (RSPN) it is the hub of over 670 bird species and about 415 resident bird species which are altitudinal refugees. Known as the East Himalayan 'hot spot' for its biological diversity, Bhutan is also a safe haven to about 16 bird species endangered globally. 
                One of the ideal areas to witness the avian life is Yongkola which is just few kilometers away from Lingmethang. 
            </p>
          </div>
        </div>
      </div>
    </div>
  </div>
  <section>
    <div class="container1">
      <div class="ground_rule">
        <h1 class="heading text-large">Our Goals: </h1>
        <p>•	To assist in the promotion and conservation of birdlife in our vicinity. </p>
        <p>•	To integrate technology solutions to an immediate problem of maintaining birdlife data and the management thereof. </p>
        <p>•	 To inculcate awareness about our avian treasures within Bhutan and beyond</p>
        <p>•	To encourage promotion of the birdlife by deploying it live.</p>
        
      </div>
    </div>
  </section>

<div class="events">
    <div class="row row-cols-1 row-cols-md-2 g-4">
        <div class="col">
            <div class="card">
                <video src="try.mp4" autoplay loop controls height="350" width="100%"></video>
                <h5 class="card-title">Yongkola Birdlife</h5>
                <!-- <p class="card-text"></p> -->
            </div>
        </div>
        <div class="col">
            <div class="card">
                <video src="try.mp4" autoplay loop controls height="350" width="100%"></video>
                <h5 class="card-title">Birds</h5>
                <!-- <p class="card-text"></p> -->
            </div>
        </div>
        
    </div>
    
  
</div>

<footer class="footer">
    <div class="row">
      <div class="col-sm-4">
        <h5>Location</h5>
        Mongar: BHUTAN
        <br>Yongkola Bird life Management
      </div>
      <div class="col-sm-4">
        <h5>Follow Us</h5>
        <div class="social-links">
          <a href="#"><i class="fab fa-facebook-f px-1"></i></a>
          <a href="#"><i class="fab fa-twitter px-1"></i></a>
          <a href="#"><i class="fab fa-instagram px-1"></i></a>
          <a href="#"><i class="fab fa-tiktok px-1"></i></a>
        </div>
      </div>
      <div class="col-sm-4">
        <h5>Contact Us</h5>
        <address>
          <a href="">Yongkolabirdlife.com</a><br>
          <hr>
          <a href="tel:17777777">17777777</a>
        </address>
      </div>
    </div>
  </footer>
  

</body>


</html>